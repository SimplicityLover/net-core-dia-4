﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CeroUnoBank.Models
{
    public class Customer : IdentityUser
    {
        public string Name { get; set; }
        public List<Account> Accounts { get; set; }
    }
}
