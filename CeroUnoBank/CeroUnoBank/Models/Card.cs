﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CeroUnoBank.Models
{
    public class Card
    {
        public int Id { get; set; }
        public string CardNumbers { get; set; }
        public string Pin { get; set; }
        public Account Account { get; set; }
    }
}
