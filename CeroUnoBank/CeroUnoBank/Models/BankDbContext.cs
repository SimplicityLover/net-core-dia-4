﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CeroUnoBank.Models
{
    public class BankDbContext : IdentityDbContext<Customer>
    {
        public BankDbContext( DbContextOptions<BankDbContext> options ) : base( options )
        {

        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}
