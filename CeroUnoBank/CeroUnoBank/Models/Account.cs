﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CeroUnoBank.Models
{
    public class Account
    {
        public int Id { get; set; }
        public decimal Balance { get; set; }
        public Customer Customer { get; set; }
        public List<Card> Card { get; set; }
    }
}
