﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CeroUnoBank.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CeroUnoBank.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private UserManager<Customer> UserManager { get; set; }
        private SignInManager<Customer> SignInManager { get; set; }
        public IConfiguration Configuration { get; set; }

        public AccountController( UserManager<Customer> um , SignInManager<Customer> sm , IConfiguration configuration )
        {
            this.UserManager = um;
            this.SignInManager = sm;
            this.Configuration = configuration;
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create( [FromBody] CreateUserModel Model  ) {

            if ( !TryValidateModel(Model) ) return BadRequest( Model );
            //if ( !ModelState.IsValid) return BadRequest(Model);

            var user = new Customer { UserName = Model.Email , Name = Model.Name , Email = Model.Email };
            var result = await UserManager.CreateAsync( user , Model.Password );

            if ( !result.Succeeded ) return BadRequest( Model );

            return CreateToken( user.Email );
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginUserModel Model)
        {

            if (!TryValidateModel(Model)) return BadRequest(Model);
            //if ( !ModelState.IsValid) return BadRequest(Model);
                        
            var result = await SignInManager.PasswordSignInAsync( Model.Email , Model.Password , true , false );

            if (!result.Succeeded) return BadRequest(Model);

            return CreateToken( Model.Email );
        }

        public IActionResult CreateToken( string Email ) {
            //[] Claims
            var claims = new Claim[] {
                new Claim( JwtRegisteredClaimNames.UniqueName , Email )
            };

            //Key
            var key = new SymmetricSecurityKey( Encoding.UTF8.GetBytes( Configuration["CEROUNOBANK_JWT_KEY"] ) );

            //Credentials
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256 );

            var expiration = DateTime.UtcNow.AddHours( 1 );

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: "CeroUnoBank",
                audience: "CeroUnoBankCustomers",
                claims: claims,
                signingCredentials: credentials,
                expires: expiration
            );

            var TokenString = new JwtSecurityTokenHandler().WriteToken(token);

            return Ok( new { token = TokenString , expiration } );
        }

    }
}