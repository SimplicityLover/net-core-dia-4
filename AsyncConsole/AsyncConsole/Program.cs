﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncConsole
{
    class Program
    {
        public static string[] Sites { get; set; }
       

        static void Main(string[] args)
        {
            Sites = new string[] {
                "https://google.com",
                "https://facebook.com",
                "https://reddit.com",
                "https://9gag.com",
                "https://freegeoip.net",
                "https://microsoft.com"
            };
            
            Console.WriteLine( "Waiting ReadLine" );
            string command = Console.ReadLine();
            while ( command.Equals( "1" ) ) {
                //RunSync();
                //Task.Run( () => RunAsync() );
                Task.Run(() => RunInParallelAsync());
                command = Console.ReadLine();
            }
        }

        static void RunSync() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            watch.Start();

            foreach ( string Site in Sites ) {
                //var str = (new WebClient()).DownloadString( Site );
                SomeLongMethodSync();
                Console.WriteLine( $"{Site} downlaoded" );
            }

            watch.Stop();
            Console.WriteLine( $"Tiempo total Sync: {watch.ElapsedMilliseconds/1000.0f}" );
        }

        static async Task RunAsync() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            watch.Start();

            foreach (string Site in Sites)
            {
                //var str = await Task.Run( () => (new WebClient()).DownloadString( Site ) );
                await SomeLongMethod();
                Console.WriteLine($"{Site} downlaoded");
            }

            watch.Stop();
            Console.WriteLine($"Tiempo total Async: {watch.ElapsedMilliseconds / 1000.0f}");
        }

        static async Task RunInParallelAsync() {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            watch.Start();

            List<Task> ListOfTasks = new List<Task>();

            foreach (string Site in Sites)
            {
                ListOfTasks.Add( SomeLongMethod() );                
            }

            await Task.WhenAll( ListOfTasks );
                        
            for (int i = 0 ; i < ListOfTasks.Count; i++ ) {
                Console.WriteLine($"{Sites[i]} Downlaoded");
            }

            watch.Stop();
            Console.WriteLine($"Tiempo total Async In Parallel: {watch.ElapsedMilliseconds / 1000.0f}");
        }

        public static void SomeLongMethodSync()
        {
            Thread.Sleep( 2000 );
        }

        public async static Task SomeLongMethod() {
            await Task.Delay( 2000 );
        }

    }
}
